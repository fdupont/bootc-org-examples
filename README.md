# Example containers

This repository contains example bootable containers.

## Examples

- [nvidia](nvidia/): Install the nvidia driver
- [tailscale](tailscale/): Demos <https://tailscale.com/download/linux/fedora>
- [wifi](wifi/): Install support for wireless networks along with pre-baked
  configuration to join a network
- [httpd](httpd/): Run an apache webserver
- [microshift](microshift/): Run MicroShift: a lightweight K8s distribution for the edge

## Other useful repositories

- <https://github.com/coreos/layering-examples>
- <https://github.com/openshift/rhcos-image-layering-examples/>
